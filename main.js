export function setup(ctx) {
    ctx.onCharacterSelectionLoaded(ctx => {
        // debug
        const debugLog = (...msg) => {
            mod.api.SEMI.log(`${id} v4501`, ...msg);
        };

        // variables
        const id = "semi-auto-town-trader";
        const name = "SEMI Auto Town Trader";

        let config = {
            townshipItemDict: {},
            convertPercent: 50,
            enabled: false
        };

        const TOWNSHIP_RESOURCES = game.township.resources.allObjects;
        TOWNSHIP_RESOURCES.forEach(resource => {
            config.townshipItemDict[resource.id] = null;
        });

        const isItemSelected = (itemid, resourceid) => config.townshipItemDict[resourceid] === itemid;
        const toggleItem = (itemid, resourceid) => {
            if (config.townshipItemDict[resourceid] === itemid) {
                config.townshipItemDict[resourceid] = null;
                return;
            }

            config.townshipItemDict[resourceid] = itemid;
        };

        // modal
        const injectModal = () => {
            // Overlay Modal
            const scriptModal = mod.api.SEMI.buildModal(id, `${name}`);
            scriptModal.blockContainer.html(`
                <div class="block-content pt-0 font-size-sm">
                    <div class="row semi-grid xs pt-1 mb-4">
                        <div class="col-md-5 col-sm-12">
                            <div class="w-100 mb-3">
                                <div class="custom-control custom-switch custom-control-lg">
                                    <input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
                                    <label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 mb-2">
                            <div class="w-100 pl-1">Convert All Percent:
                                <button type="button" class="btn btn-sm btn-secondary dropdown-toggle ml-1" id="${id}-convert_percent_button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">${config.convertPercent}%</button>
                                <div class="dropdown-menu font-size-sm" x-placement="bottom-start" id="${id}-convert_percent"></div>        
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="w-100 semi-grid-item mb-1 disable">
                                Locked
                            </div>
                            <div class="w-100 semi-grid-item mb-1">
                                Disabled
                            </div>
                            <div class="w-100 semi-grid-item enable">
                                Enabled
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block-content pt-0 font-size-sm">
                    <div class="pb-4" id="${id}-container"></div>
                </div>
            `);

            const convertPercentButton = document.getElementById(id + '-convert_percent_button');
            game.township.convertValues.percentages.forEach((value) => {
                const newOption = createElement('a', {className: 'dropdown-item pointer-enabled', text: `${value}%`});
                newOption.onclick = () => {
                    convertPercentButton.innerText = `${value}%`
                    config.convertPercent = value;
                };
                $(`#${id}-convert_percent`).append(newOption)
            });

            $(scriptModal.modal).on('hidden.bs.modal', () => {
                clearContainer();
                storeConfig();

                processTrader();
            });

            $(`#${id}-enable-check`).on('change', function (e) {
                toggleEnabledStatus();
            });

            $(`#${id}-enable-check`).prop('checked', config.enabled);

        }

        const showModal = () => {
            updateModal();
            $(`#modal-${id}`).modal('show');
        };

        const updateModal = () => {
            clearContainer();
            buildResources();
        };

        const clearContainer = () => {
            $(`#${id}-container`).empty();
        };

        const toggleEnabledStatus = () => {
            config.enabled = !config.enabled;
            storeConfig();
        };

        const disableItem = (itemid) => {
            let elm = document.getElementById(id + '-' + itemid);
            elm.classList.toggle("enable")
        }

        const buildResources = () => {
            $(`#${id}-container`).html(TOWNSHIP_RESOURCES.map(resource => buildResource(resource)).join(''));

            tippy(`#${id}-container [data-tippy-content]`, {
                animation: false,
                allowHTML: true
            });

            $(`#${id}-container .semi-grid-item`).on('click', function (e) {
                const elm = $(this);
                if (elm.hasClass('disable')) {
                    return;
                }

                const itemid = elm.data("item");
                const resourceid = elm.data("resource");
                const townshipItem = config.townshipItemDict[resourceid];
                if (townshipItem != null && townshipItem !== itemid) {
                    disableItem(townshipItem);
                }

                elm.toggleClass('enable');
                toggleItem(itemid, resourceid);
            });
        }

        const buildResource = (resource) => {
            let conversionList = game.township.getResourceItemConversionsFromTownship(resource);
            if (conversionList.length === 0) {
                return '';
            }

            return `
            <div class="block block-rounded-double bg-combat-inner-dark pr-1 px-1 mb-1 mt-1"">
                 <img class="skill-icon-sm mr-2" src="${resource.media}">
                 <span class="font-w600">${resource.name}</span>
            </div>
            <div class="row pl-1">
                <div class="col semi-grid sm">
                    ${conversionList.map(conversion => buildConversionItem(conversion, resource)).join('')}
                </div>
            </div>`;
        }


        const buildConversionItem = (conversion, resource) => {
            const item = conversion.item;
            const STATE_CLASS = isRequirementMet(conversion.unlockRequirements) ? (isItemSelected(item.id, resource.id) ? 'enable' : '') : 'disable';

            return `<div id="${id}-${item.id}" class="semi-grid-item float m-1 ${STATE_CLASS}" data-item="${item.id}" data-resource="${resource.id}" data-tippy-content="${item.name}">
            <img src="${item.media}" alt="${item.name}" />
        </div>`;

        }

        const processTrader = () => {
            if (!config.enabled || !game.township.isStorageFull) {
                return;
            }
            TOWNSHIP_RESOURCES.forEach((resource) => {
                if (config.townshipItemDict[resource.id] === null) {
                    return;
                }

                let item = game.items.getObjectByID(config.townshipItemDict[resource.id]);
                game.township.convertQtyType = 1;
                game.township.convertQtyPercent = config.convertPercent;
                game.township.updateConvertFromQty(0, resource, item);
                game.township.processConversionFromTownship(item, resource);

            });
        }

        // config
        const storeConfig = () => {
            ctx.characterStorage.setItem('config', config);
        }

        const loadConfig = () => {
            const storedConfig = ctx.characterStorage.getItem('config');
            if (!storedConfig) {
                return;
            }

            config = {...config, ...storedConfig};
        }

        // hooks + game patches
        ctx.onCharacterLoaded(ctx => {
            loadConfig();

            processTrader();

            ctx.patch(Township, 'tick').after(function () {
                try {
                    processTrader();
                } catch (e) {
                    debugLog(e);
                }
            });
        });

        ctx.onInterfaceReady(() => {
            injectModal();
            mod.api.SEMI.addSideBarModSetting(name, ctx.getResourceUrl('semi_icon.png'), showModal);
        });
    });
}
